import './App.css';
import Buttons from './assets/js/Buttons';
import Messages from './assets/js/Messages';

function App() {
  return (
    <div id="App" className="App">
      <Buttons />
      <Messages />
    </div>
  );
}

export default App;
