import App from "../../App";
import data from "../../json/data.json";

function getMessage(data, type) {
    if( data.is_active === true ) {
        return (
            <div id={`${type}_message`} className="alert">
                {data.message}
                {closeButton(data.is_important)}
            </div>
        );
    }
    return;
}

function Messages(props) {
    return (
        <div className="messages">
            {getMessage(data.success, "success")}

            {getMessage(data.info, "info")}

            {getMessage(data.warning, "warning")}

            {getMessage(data.error, "error")}
        </div>
    );
}

function closeButton(is_important) {
    if( is_important === true ) {
        return (
            <span className="close" onClick={(evt) => {
                closeMessage(evt);
            }}>×</span>
        );
    }
    return;
}

function closeMessage(evt) {
    const alert = evt.target.parentNode;
    alert.style.opacity = '0';
}

export default Messages;