import App from "../../App";
import Messages from "./Messages";
import data from "../../json/data.json";

function getButton(data, type) {
    if (data.is_active === true ) {
        return (
            <button id={type} className="button" onClick={(evt) => {
                showHideMessage(evt, data.is_important);
            }}>{data.button_text}</button>
        );
    }
    return;
}

function Buttons() {
    return (
        <div id="buttons" className="messages-buttons">
            {getButton(data.success, "success")}

            {getButton(data.info, "info")}

            {getButton(data.warning, "warning")}

            {getButton(data.error, "error")}
        </div>
    );
}

function showHideMessage(evt, is_important) {
    const id = evt.target.getAttribute('id');
    const alert = document.getElementById(`${id}_message`);
    alert.style.opacity = '1';
    if (is_important === false) {
        const myTimeout = setTimeout(function () {
            alert.style.opacity = 0;
            clearTimeout(myTimeout);
        }, 3000);
    }    
}

export default Buttons;